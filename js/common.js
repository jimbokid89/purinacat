'use strict';
if (!window.console) window.console = {};
if (!window.console.memory) window.console.memory = function() {};
if (!window.console.debug) window.console.debug = function() {};
if (!window.console.error) window.console.error = function() {};
if (!window.console.info) window.console.info = function() {};
if (!window.console.log) window.console.log = function() {};

// sticky footer
//-----------------------------------------------------------------------------
if (!Modernizr.flexbox) {
  (function() {
    var
      $pageWrapper = $('#page-wrapper'),
      $pageBody = $('#page-body'),
      noFlexboxStickyFooter = function() {
        $pageBody.height('auto');
        if ($pageBody.height() + $('#header').outerHeight() + $('#footer').outerHeight() < $(window).height()) {
          $pageBody.height($(window).height() - $('#header').outerHeight() - $('#footer').outerHeight());
        } else {
          $pageWrapper.height('auto');
        }
      };
    $(window).on('load resize', noFlexboxStickyFooter);
  })();
}
if (ieDetector.ieVersion == 10 || ieDetector.ieVersion == 11) {
  (function() {
    var
      $pageWrapper = $('#page-wrapper'),
      $pageBody = $('#page-body'),
      ieFlexboxFix = function() {
        if ($pageBody.addClass('flex-none').height() + $('#header').outerHeight() + $('#footer').outerHeight() < $(window).height()) {
          $pageWrapper.height($(window).height());
          $pageBody.removeClass('flex-none');
        } else {
          $pageWrapper.height('auto');
        }
      };
    ieFlexboxFix();
    $(window).on('load resize', ieFlexboxFix);
  })();
}

$(function() {

  // placeholder
  //-----------------------------------------------------------------------------
  $('input[placeholder], textarea[placeholder]').placeholder();

  $('.chose-animal .form-group').each(function(e){
    $(this).find('input[type=checkbox]').attr('id','animal'+e).parent().append('<label for="animal'+e+'"></label>');
  })

  $('.chose-animal .radio').each(function(e){
    $(this).find('input[type=radio]').attr('id','animal'+e).parent().append('<label for="animal'+e+'"></label>');
  })

  //$('.chose-animal input[type=radio]').attr('id','animal').parent().append('<label for="animal"></label>');

  $(document).on('click','.js-remove-animal',function(e){
    e.preventDefault();
    $('.chose-animal input').prop('checked', false);
  });

  //$(document).on('click','.js-add-animal',function(e){
  //  e.preventDefault();
  //  $('.animal-form.base').clone().insertAfter('.animal-form:last');
  //  $('.animal-form.new').removeClass('new');
  //  $('.register-form.step-3 .animal-form:last').removeClass('base').addClass('new').find('input').val([]);
  //
  //  $('.animal-form.new > div').each(function(){
  //    var parent = $(this);
  //    parent.find('select.select-style').insertAfter($(this).find('.title-input'));
  //    parent.find('div.select-style').remove();
  //    parent.find('select.select-style').styler();
  //  })
  //
  //  $('.animal-form.new .checkboxes').remove();
  //})
});


$(window).on('load', function() {
  $('.grid').masonry({
    itemSelector: '.grid-item',
    columnWidth: '.grid-sizer'
  });


  if ($('.photo-row').length != 0) {
    $('.photo-row').matchHeight({
      byRow: true,
      property: 'height'
    });

    $('.photo-row').each(function() {
      var photo = $(this).find('.photo-wrap >img');
      if (photo.height() < photo.width()) {
        photo.addClass('horizontal');
      }
    });
  }
});

// Pop Up FN
var overlay = $('.pop-up-overlay'),
  popUpWrapIdentify = '.pop-ups',
  popUpContentIdentify = '.pop-ups-content',
  popUpIdentify = popUpWrapIdentify + ' .pop-up',
  duration = 400;

$(document).on('click', '.js-close-pop-up, .pop-up-overlay', function(e) {
  e.preventDefault();
  closePop();
});

$('body').keydown(function(e) {
  if (e.which === 27) { //Click on ESC btn
    closePop();
  }
});

function closePop() {

  var close = true;

  var popups = $(popUpIdentify);
  popups.each(function() {

    var popup = $(this);
    if (popup.is(':visible')) {

      if (loadPopUp(popup.data('close-pop-url'), popup.data('close-pop'))) {

        popup.data('close-pop', null);
        popup.data('close-pop-url', null);

        return close = false;
      }
    }

  });

  if (close) {
    popups.fadeOut(duration);
    overlay.fadeOut(duration);
  }

}

function loadPopUp(url, typePop) {

  if (url && typePop) {

    var funcHidePreviosPopUp = function() {

      $(popUpIdentify).each(function() {
        if ($(this).is(':visible')) {
          $(this).fadeOut(duration);
        }
      });

    };

    var popUp = $(popUpWrapIdentify + ' .' + typePop);
    if (popUp && popUp.length) {

      var form = popUp.find('form');

      if (form.length) {

        form.find('input[type=text],input[type=password],select').each(function() {

          $(this).val('');

          if ($(this).is('select') && $(this).parent().hasClass('jqselect')) {
            $(this).trigger('refresh');
          }

        });

        if (typeof form.yiiActiveForm != "undefined") {
          form.yiiActiveForm('resetForm');
        }

      }

      funcHidePreviosPopUp();
      showPop(typePop, popUp);

    } else {

      var contentWrap = $(popUpWrapIdentify + ' ' + popUpContentIdentify);
      var indetify = popUpContentIdentify + '-' + typePop;
      var content = contentWrap.find(indetify);

      if (!content || !content.length) {
        content = $('<div/>', {
          class: indetify.substring(1)
        }).appendTo(contentWrap);
      }

      $(content).load(url, function() {
        var jqXHR = arguments[2];
        if (jqXHR.status == 200) {

          funcHidePreviosPopUp();

          formStyler(indetify);

          showPop(typePop);
        }
      });

    }

    return true;

  }

  return false;

}
$(document).on('click', '.js-show-pop-up', function(e) {

  loadPopUp($(this).data('url'), $(this).data('atr'));

  e.preventDefault();

});


function showPop(typePop, popUp) {

  var funcShow = function() {

    popUp.show();
    var popUpHeight = popUp.outerHeight();
    popUp.hide();

    var top = ($(window).height() - popUpHeight) / 2 + $(document).scrollTop();
    var documentHeight = $(document).height();

    if (top + popUpHeight > documentHeight) {
      top -= top + popUpHeight - documentHeight;
    }

    if (top < 0) {
      top = 40;
    }

    popUp.fadeIn(duration).css({
      top: top
    });

    overlay.fadeIn(duration);

  };

  if (!popUp) {

    popUp = $(popUpWrapIdentify + ' .' + typePop);

    var images = popUp.find("img");
    if (images.length) {

      images.first().on("load", function() {
        funcShow();
      });

      return;
    }

  }

  funcShow();
}


function formStyler(identify) {

  var elements = [
    '.select-style',
    '.checkbox-wrap input',
    '.input-style'
  ];

  if (identify && $.type(identify) == 'object') {

    $(elements).each(function() {
      $(identify).find(this).styler();
    });

  } else {

    if (!identify) {
      identify = '';
    } else {
      identify += ' ';
    }

    $(elements).each(function() {
      $(identify + this).styler();
    });

  }

}

//sticky sidebar
var sidebar = $('.sidebar');
var sidebarPos = sidebar.offset().top;
var headerHeight = $('header').outerHeight();


// $(window).on('scroll load resize', function() {
//   if (!($(window).outerHeight() < 520) && ($(window).outerWidth() < 991)) {
//     if ($(window).scrollTop() > headerHeight) {
//       sidebar.css({
//         'position': 'fixed'
//       })
//     } else {
//       sidebar.css({
//         'position': 'absolute'
//       })
//     }
//   }
// })

//Sidebar
$('.js-show-sidebar').on('click', function(e) {
  e.preventDefault();
  if ($(this).hasClass('active')) {
    closeSideBar();
  } else {
    $('.sidebar').addClass('open');
    $(this).addClass('active');
    $('.overlay').fadeIn();
  }
});

$('.overlay').on('click', function() {
  closeSideBar();
})

function closeSideBar() {
  $('.sidebar').removeClass('open');
  $('.js-show-sidebar').removeClass('active');
  $('.overlay').fadeOut();
}

$('.birthday input').datepicker({
  autoclose: true,
});

$('#sandbox-container input').on('show', function(e) {
  console.debug('show', e.date, $(this).data('stickyDate'));
  if (e.date) {
    $(this).data('stickyDate', e.date);
  } else {
    $(this).data('stickyDate', null);
  }
});

$('#sandbox-container input').on('hide', function(e) {
  console.debug('hide', e.date, $(this).data('stickyDate'));
  var stickyDate = $(this).data('stickyDate');

  if (!e.date && stickyDate) {
    console.debug('restore stickyDate', stickyDate);
    $(this).datepicker('setDate', stickyDate);
    $(this).data('stickyDate', null);
  }
});

$("#phone").mask("+7(999) 9999-999");
//$('.select-style,.checkbox-wrap input,.input-style').styler();

//Jquery AutoComplete

var availableTags = [
  "ActionScript",
  "AppleScript",
  "Asp",
  "BASIC",
  "C",
  "C++",
  "Clojure",
  "COBOL",
  "ColdFusion",
  "Erlang",
  "Fortran",
  "Groovy",
  "Haskell",
  "Java",
  "JavaScript",
  "Lisp",
  "Perl",
  "PHP",
  "Python",
  "Ruby",
  "Scala",
  "Scheme"
];

$("#autocomplete-region").autocomplete({
  source: availableTags,
  autoFocus: true,
  minLength: 0
}).focus(function() {
  $(this).autocomplete("search", "");
});

$("#autocomplete-town").autocomplete({
  source: availableTags,
  autoFocus: true,
  minLength: 0
}).focus(function() {
  $(this).autocomplete("search", "");
});

$("#autocomplete-street").autocomplete({
  source: availableTags,
  autoFocus: true,
  minLength: 0
}).focus(function() {
  $(this).autocomplete("search", "");
});

$(document).ready(function() {
  formStyler();

  $('.check-wrap').change(function() {
    var id = $(this).find('input').attr('id'),
      workWrap = $('.work-wrap .preview');
    if (id === 'check-2') {
      workWrap.removeClass('border-green').addClass('border-grey')
    } else if (id === 'check-3') {
      workWrap.removeClass('border-grey').addClass('border-green')
    } else {
      workWrap.removeClass('border-grey border-green');
    }
  });

  function checkSize() {
    var imageUser = $('.preview .user-image');
    $('.preview').removeClass('horizontal vertical');
    if (imageUser.width() > imageUser.height()) {
      $('.preview').addClass('horizontal');
    } else {
      $('.preview').addClass('vertical');
    }
  }

  function readURL(input) {

    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $('.user-image').remove();
        $('.preview').append('<img src="#" alt="" class="user-image">')
        $('.user-image').attr('src', e.target.result);

        if ($('.user-image').on('load')) {
          checkSize();
        };

      }

      reader.readAsDataURL(input.files[0]);
    }
  }

  $("#creativeimageaddform-image").change(function() {
    readURL(this);
  });

  $('#fileInput').change(function() {
    var files = $('#fileInput').get(0).files;
    for (var i = 0, l = files.length; i < l; i++) {
      console.log(files[i].name);
      $('.file-list').append('<p>'+files[i].name+'</p>');
    }
  })

  $(".fancybox").fancybox();

});
